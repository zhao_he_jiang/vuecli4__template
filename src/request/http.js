// //二次封装
// import { Request } from "@/until/request.js"

// export const banner =  data => Request.getData({
//     url: `https://test.365msmk.com/api/app/banner`,
//     method: "get",
//     data
// })

// export const index =  data => Request.getData({
//     url: `https://test.365msmk.com/api/app/recommend/appIndex`,                  //首页
//     method: "get",
//     data
// })

// export const course = (obj,data) => Request.getData({
//     url: `https://test.365msmk.com/api/app/courseBasis?page=${obj.page}&limit=${obj.limit}&course_type=${obj.course_type}&classify_id=${obj.classify_id}&order_by=${obj.order_by}&attr_val_id=${obj.attr_val_id}&is_vip=${obj.is_vip}`,    //特色课
//     method: "get",
//     data
// })
// export const search = (obj,data) => Request.getData({
//     url: `https://test.365msmk.com/api/app/courseBasis?limit=${obj.limit}&page=${obj.page}&course_type=${obj.course_type}&keywords=${obj.keywords}`,    //特色课--搜索
//     method: "get",
//     data
// })
// // export const search = p => get(`api/app/courseBasis?page=${p.page}&limit=${p.limit}&course_type=${p.course_type}&keywords=${p.keywords}`, p);
// export const list =  data => Request.getData({
//     url: `https://test.365msmk.com/api/app/courseInfo/basis_id=${data.id}`,    //特色课
//     method: "get",
//     data
// })
// export const getClassify =  data => Request.getData({
//     url: `https://test.365msmk.com/api/app/courseClassify`,    //分类
//     method: "get",
//     data
// })

// export const courseClassify =  data => Request.getData({
//     url: `https://test.365msmk.com/api/app/courseClassify`,
//     method: "get",
//     data
// })
// export const list1 =  (obj,data) => Request.getData({
//     url: `https://test.365msmk.com/api/app/teacher/info=${obj.id}`,
//     method: "get",
//     data
// })

// export const login = (data) => Request.getData({
//     url: `https://test.365msmk.com/api/app/login`,
//     method: "POST",
//     params: data
// })

// export const SmsVerification = (obj,data) => Request.getData({                                    //发送验证码
//     url: `https://test.365msmk.com/api/app/smsCode?mobile=${obj.mobile}&sms_type=${obj.sms_type}`,
//     method: "POST",
// })
// export const VerificationLogin = (obj,data) => Request.getData({                                    //验证码登录
//     url: `https://test.365msmk.com/api/app/login?mobile=${obj.mobile}&sms_code=${obj.sms_code}&type=${obj.type}`,
//     method: "POST",
// })

// export const password = (obj,data) => Request.getData({                                    //验证码登录
//     url: `https://test.365msmk.com/api/app/password`,
//     method: "POST",
// })

// export const attr = (obj,data) => Request.getData({                                    //验证码登录
//     url: `https://test.365msmk.com/api/module/attribute/1`,
//     method: "get",
// })
// export const Userinfo = (obj,data) => Request.getData({                                    //验证码登录
//     url: `https://test.365msmk.com/api/app/userInfo`,
//     method: "get",
// })

// export const nickname = (obj,data) => Request.getData({                                    //验证码登录
//     url: `https://test.365msmk.com/api/app/user?nickname=${obj.nickname}`,
//     method: "put",
// })


// export const omit = (obj,data) => Request.getData({                                    
//     url: `https://test.365msmk.com/api/app/sonArea/0`,
//     method: "get",
// })

// export const City = (obj,data) => Request.getData({                                    
//     url: `https://test.365msmk.com/api/app/sonArea/${obj}`,
//     method: "get",
// })

// export const share = (obj,data) => Request.getData({                                    
//     url: `https://test.365msmk.com/api/app/public/share`,
//     method: "post",
// })


// export const UserCenter = (obj,data) => Request.getData({                                    
//     url: `https://test.365msmk.com/api/app/getUCenterInfo`,
//     method: "get",
// })
// export const user = (obj,data) => Request.getData({                                    
//     url: `https://test.365msmk.com/api/app/user`,
//     method: "put",
// })
// //更换用户头像
// export const avatar = (obj,data) => Request.getData({                                    
//     url: `https://test.365msmk.com/api/app/public/img`,
//     method: "post",
// })
// export const oto = (obj,data) => Request.getData({                                    
//     url: `https://test.365msmk.com/api/app/otoCourse`,
//     method: "get",
// })
// export const otoconditon = (obj,data) => Request.getData({                                    
//     url: `https://test.365msmk.com/api/app/otoCourseOptions`,
//     method: "get",
// })
// export const mystaus = (obj,data) => Request.getData({                                    
//     url: `https://test.365msmk.com/api/app/myStudy/${obj}`,
//     method: "get",
// })

// export const coinRank = (obj,data) => Request.getData({                                    
//     url: `https://test.365msmk.com/api/app/coin/coinRank`,
//     method: "get",
// })
// export const coinBalance = (obj,data) => Request.getData({                                    
//     url: `https://test.365msmk.com/api/app/coinBalance`,
//     method: "get",
// })
// export const pay = (obj,data) => Request.getData({                                    
//     url: `https://test.365msmk.com/api/app/pay`,
//     method: "post",
// })

// export const item = (obj,data) => Request.getData({                                    
//     url: `https://test.365msmk.com/api/app/coin/item`,
//     method: "get",
// })
// export const wxUrl = (obj,data) => Request.getData({                                    
//     url: `https://test.365msmk.com/api/app/pay/wxUrl?coin_id=${obj.coin_id}`,
//     method: "get",
// })

// export const UserCenter = () => thia.$http.get('/getUCenterInfo')



/**axios封装
 * 请求拦截、相应拦截、错误统一处理
 */
import axios from 'axios';
import QS from 'qs';
import { Toast } from 'vant';
import store from '../store/index'

// 环境的切换
if (process.env.NODE_ENV == 'development') {
    axios.defaults.baseURL = 'https://test.365msmk.com/';
} else if (process.env.NODE_ENV == 'debug') {
    axios.defaults.baseURL = '';
} else if (process.env.NODE_ENV == 'production') {
    axios.defaults.baseURL = 'http://api.123dailu.com/';
}

// 请求超时时间
axios.defaults.timeout = 10000;

// post请求头
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8';

// 请求拦截器
axios.interceptors.request.use(
    config => {
        // 每次发送请求之前判断是否存在token，如果存在，则统一在http请求的header都加上token，不用每次请求都手动添加了
        // 即使本地存在token，也有可能token是过期的，所以在响应拦截器中要对返回状态进行判断
        const token = store.state.token;
        token && (config.headers.Authorization = token);
        return config;
    },
    error => {
        return Promise.error(error);
    })

// 响应拦截器
axios.interceptors.response.use(
    response => {
        if (response.status === 200) {
            return Promise.resolve(response);
        } else {
            return Promise.reject(response);
        }
    },
    // 服务器状态码不是200的情况
    error => {
        if (error.response.status) {
            switch (error.response.status) {
                // 401: 未登录
                // 未登录则跳转登录页面，并携带当前页面的路径
                // 在登录成功后返回当前页面，这一步需要在登录页操作。
                case 401:
                    router.replace({
                        path: '/login',
                        query: { redirect: router.currentRoute.fullPath }
                    });
                    break;
                // 403 token过期
                // 登录过期对用户进行提示
                // 清除本地token和清空vuex中token对象
                // 跳转登录页面
                case 403:
                    Toast({
                        message: '登录过期，请重新登录',
                        duration: 1000,
                        forbidClick: true
                    });
                    // 清除token
                    localStorage.removeItem('token');
                    store.commit('loginSuccess', null);
                    // 跳转登录页面，并将要浏览的页面fullPath传过去，登录成功后跳转需要访问的页面
                    setTimeout(() => {
                        router.replace({
                            path: '/login',
                            query: {
                                redirect: router.currentRoute.fullPath
                            }
                        });
                    }, 1000);
                    break;
                // 404请求不存在
                case 404:
                    Toast({
                        message: '网络请求不存在',
                        duration: 1500,
                        forbidClick: true
                    });
                    break;
                // 其他错误，直接抛出错误提示
                default:
                    Toast({
                        message: error.response.data.message,
                        duration: 1500,
                        forbidClick: true
                    });
            }
            return Promise.reject(error.response);
        }
    }
);
/**
 * get方法，对应get请求
 * @param {String} url [请求的url地址]
 * @param {Object} params [请求时携带的参数]
 */
export function get(url, params){
    return new Promise((resolve, reject) =>{
        axios.get(url, {
            params: params
        })
            .then(res => {
                resolve(res.data);
            })
            .catch(err => {
                reject(err.data)
            })
    });
}
/**
 * post方法，对应post请求
 * @param {String} url [请求的url地址]
 * @param {Object} params [请求时携带的参数]
 */
export function post(url, params) {
    return new Promise((resolve, reject) => {
        axios.post(url, QS.stringify(params))
            .then(res => {
                resolve(res.data);
            })
            .catch(err => {
                reject(err.data)
            })
    });
}