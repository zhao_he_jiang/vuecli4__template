import { get, post } from "./http"


//首页
export const index = data => get("https://test.365msmk.com/api/app/recommend/appIndex", data)
//推荐课程 进入详情页
export const list=(p)=>get(`https://test.365msmk.com/api/app/courseInfo/basis_id=${p.id}`,p)

export const list1=(p)=>get(`https://test.365msmk.com/api/app/teacher/info=${p.id}`,p)


//课程
export const course =data=>get("https://test.365msmk.com/api/app/courseBasis?page=7&limit=10",data)




//首页
export const List = p => get('api/app/recommend/appIndex', p);
// 详情页
export const detail = p => get(`api/app/courseInfo/basis_id=${p}`, p);
// 课程页
export const classify = p => get(`api/app/courseBasis?page=${p}&limit=10`, p);

export const page = p => get(`api/app/courseClassify`, p);
// 搜索
export const search = p => get(`api/app/courseBasis?page=${p.page}&limit=${p.limit}&course_type=${p.course_type}&keywords=${p.keywords}`, p);

// 登录接口
export const Login = p => post(`/api/app/login`, p);
// 验证码接口
export const yanMA = p => post(`/api/app/smsCode`, p);
// 修改密码接口
export const Pass = p => post(`/api/app/password`, p);