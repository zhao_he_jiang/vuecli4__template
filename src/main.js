import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import 'lib-flexible'
import FastClick from 'fastclick'
import VueScroller from 'vue-scroller'
import axios from 'axios';
import Vant from 'vant';
import 'vant/lib/index.css';

Vue.config.productionTip = false
FastClick.attach(document.body)
Vue.use(VueScroller)
Vue.prototype.axios = axios;
Vue.use(Vant);



new Vue({
  // el:'#app',   //自动挂载
  router,
  store,
  render: h => h(App)
}).$mount('#app')
// .$mount('#app')    //手动挂载